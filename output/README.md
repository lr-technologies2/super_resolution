# output
This is where ouput files (figures, txt...) are saved by default.
All files here are temporary and can be deleted when running the application.
To permanently save a file, move it into another subfolder.