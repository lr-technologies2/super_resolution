# models/tmp
This is where files get saved when performing training. 
All files here are temporary and can be deleted when running the application.
To permanently save a model, move it into another subfolder of /models.