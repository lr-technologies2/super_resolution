# models/debug_dataset
Models that are trained on a debug dataset can be stored here.
These models can be used for demonstration purposes on a small subset of the main datasets.
However, it is unlikely that these models will be able to generalise well.