'''
This package references all neural network classes used in the application.
Author: Adrien Dorise - Edouard Villain ({adorise, evillain}@lrtechnologies.fr) - LR Technologies
Created: September 2023
Last updated: Adrien Dorise - September 2023

'''

import src.features.preprocessing as pr
import src.features.image_preprocessing as imgpr
import dragonflai.visualisation.display_mouse_prediction as visu
import dragonflai.visualisation.draw_tracker as draw_tracker
from dragonflai.model.machineLearning import Regressor

import pickle
from sklearn.preprocessing import MinMaxScaler
import torch
from enum import Enum
import os
import cv2


class ModelType(Enum):
     MACHINE_LEARNING = 1
     NEURAL_NETWORK = 2



class Experiment():
    """Regroup both a model and the data for a complete experiment.
    It can take into account both machine learning and neural network models
    It can fit with both tracker and full image datasets. Only the paths are stored, to avoid saving the whole dataset as Python variables. Data are loaded on the fly when needed (during fit or predict).

    Args:
            model (model class (such as Regressor/NeuralNetwork): algorithm selected
            train_path (string): Folder path containing the training samples
            val_path (string): Folder path containing the validation samples
            test_path (string): Folder path containing the testing samples
            visu_path (string): Folder path containing the samples used for result visualisation
            shape: X and Y dimension of the images. They will be reshape into this parameters. Default is (254,254).
            input_type (InputType enum, optional): Type of feature used (ex: tracker, image). Defaults to InputType.IMAGE.
            crop (string, optional): Select the crop used for image input. Choices are {None, "face", "eyes"}. Defaults to None.
            coords (str, optional): Coordinates used for the tracker features. Choices are {"xy", "xyz"}. Defaults to "xyz".
            tracker_version: int, optional
                identifier of the set of keypoints to keep. Defaults to 1. 
                Options are:
                    - 0: keep all 478 points.
                    - 1: V1, keep 13 points (2pts for the sides of the face, 1pt for 
                            the chin, 4pts for the corners of the eyes, 4pts for the 
                            face vertical axis, 2pts for the corners of the mouth)
                    - 2: V2, keep 69 points (2pts for the sides of the face, 1pt for 
                            the chin, 10pts for the pupils, 32pts for the eyes, 4pts 
                            for the face vertical axis, 20 pts for the mouth contour)
            sequence_length (int, optional):  Only used when temporal set to True. Number of previous observation to take in the model. Default to 0
            num_epoch (int, optional): Amount of epochs to perform during training. Defaults to 50.
            batch_size (int, optional): batch_size used for DataLoader. Defaults to 32.
            learning_rate (int, optional): learning_rate used during backpropagation. Defaults to 1e-03.
            weight_decay (int, optional): regularisation criterion. Defaults to 1e-03.
            optimizer (torch.nn, optional): Optimizer used during training for backpropagation. Defaults to torch.optim.Adam.
            criterion (torch.optim, optional): Criterion used during training for loss calculation. Defaults to torch.nn.L1Loss().
            scaler (sklearn.preprocessing, optional): Type of scaler used for data preprocessing. Put None if no scaler needed. Defaults to MinMaxScaler().
            nb_workers (int, optional):  How many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. Default to 0. Defaults to 0.
    """
    def __init__(self, model,
                train_path,
                val_path,
                test_path,
                visu_path,
                input_shape=(254,254),
                crop=None,
                sequence_length=0,
                num_epoch=50,
                batch_size=32,
                learning_rate=1e-03,
                weight_decay=1e-03,    
                optimizer=torch.optim.Adam,
                criterion=torch.nn.L1Loss(),
                scaler = MinMaxScaler(),
                nb_workers=0):
        
        #Model parameters  
        self.model = model
        if(type(model) == Regressor):
            self.model_type = ModelType.MACHINE_LEARNING
            self.no_batch = True
        else:
            self.model_type = ModelType.NEURAL_NETWORK
            self.no_batch = False
            self.use_scheduler = True 
            self.loss_indicators = 1

        #Path parameters
        self.train_path = train_path
        self.val_path = val_path
        self.test_path = test_path
        self.visu_path = visu_path
        
        #Features parameters
        self.input_shape = input_shape
        self.scaler = scaler
        self.crop = crop

        
        #Temporal parameters
        self.shuffle = True
        self.is_temporal_model = sequence_length > 0
        self.sequence_length = sequence_length
        if(self.is_temporal_model):
            self.shuffle = False
            
        #Training parameters
        self.num_epoch = num_epoch
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.optimizer = optimizer
        self.criterion = criterion
        self.nb_workers = nb_workers
         
    def fit(self):
        """Train the model using the data available in the train and validation folder path.
        """
        # !!! Data loading !!!
        train_set = imgpr.img_loader(self.train_path, shape=self.input_shape, shuffle=self.shuffle, batch_size=self.batch_size,crop=self.crop, num_workers=self.nb_workers, temporal=self.is_temporal_model,sequence_length=self.sequence_length,classification=False)
        val_set = imgpr.img_loader(self.val_path, shape=self.input_shape, shuffle=False, crop=self.crop, num_workers=self.nb_workers, temporal=self.is_temporal_model,sequence_length=self.sequence_length,classification=False)
    

        #!!! Training!!! 
        if(self.model_type == ModelType.NEURAL_NETWORK):
            losses_train, losses_val = self.model.fit(train_set,
            self.num_epoch, 
            criterion=self.criterion, 
            optimizer=self.optimizer,
            learning_rate=self.learning_rate,
            weight_decay=self.weight_decay, 
            valid_set=val_set,
            loss_indicators = self.loss_indicators)
            self.model.plotLoss(losses_train,losses_val)


        elif(self.model_type == ModelType.MACHINE_LEARNING):
            self.model.fit(train_set)
            result, out = self.model.predict(val_set)
            print(f"Test loss for <{self.model}> is: {result}")

        self.model.saveModel(f"models/tmp/model")
        self.save(f"models/tmp/experiment")
        

    def predict(self):          
        """Model prediction on the samples available in the test folder path
        """
        # !!! Data loading !!!
        test_set = imgpr.img_loader(self.test_path, shape=self.input_shape, shuffle=False, batch_size=self.batch_size,crop=self.crop, num_workers=self.nb_workers, temporal=self.is_temporal_model,sequence_length=self.sequence_length,classification=False)
        score, prediction, _ = self.model.predict(test_set,self.criterion)



    def visualise(self):
        """Visualisation of the first picture of the test set.
        The input + predicted images are both shown.
        """
        # !!! Data loading !!!
        visu_set = imgpr.img_loader(self.visu_path, shape=self.input_shape, shuffle=False, batch_size=self.batch_size,crop=self.crop, num_workers=self.nb_workers, temporal=self.is_temporal_model,sequence_length=self.sequence_length,classification=False)
        
        score, pred, (feature, target) = self.model.predict(visu_set,self.criterion)

        feature = feature[0].transpose(1,2,0)
        feature = cv2.normalize(feature, dst=None, alpha=0, beta=255,norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        if(cv2.imwrite("output/tmp/input.png", feature)):
            print("Image saved in output/tmp/input.png")
        else:
            raise Exception("Could not write image in output/tmp/input.png")
        
        target = target[0].transpose(1,2,0)
        target = cv2.normalize(target, dst=None, alpha=0, beta=255,norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        if(cv2.imwrite("output/tmp/target.png", target)):
            print("Image saved in output/tmp/target.png")
        else:
            raise Exception("Could not write image in output/tmp/target.png")

        pred = pred[0].transpose(1,2,0)
        pred = cv2.normalize(pred, dst=None, alpha=0, beta=255,norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)        
        if(cv2.imwrite("output/tmp/prediction.png", pred)):
            print("Image saved in output/tmp/prediction.png")
        else:
            raise Exception("Could not write image in output/tmp/prediction.png")
        


    def fit_scaler(self, data_path):
        """Fit the scaler on the given samples.
        Usually done on the training set. 
        The scaler is set beforehand and used trhoughout the experiment to avoid inconsistance  in datasets

        Args:
            data_path (string): Folder path containing the fitting samples
        """
        _, self.scaler = pr.loader(data_path, shuffle = False, no_batch=True, batch_size=self.batch_size, scaler=self.scaler, coords=self.coords, num_workers=self.nb_workers, tracker_version=self.tracker_version, temporal=self.is_temporal_model, sequence_length=self.sequence_length)
    
    
    def save(self, filename):
        """Save the whole experiment class as a pickle object.

        Args:
            filename (string): Path to save the experiment status
        """
        with open(filename, 'wb') as file:
            try:
                pickle.dump(self, file)
            except EOFError:
                raise Exception("Error in save experiment: Pickle was not able to save the file.")

    @classmethod
    def load(self, filename):
        """Load a pickle object to an Experiment class Python variable
        This is a class method. It means that a reference to the class is NOT necessary to call this method. Simply type <your_experiment = Experiment.load(filename)>

        Args:
            filename (string): Path to the pickle saved object.
        """
        with open(filename, 'rb') as file:
            try:
               return pickle.load(file)
            except EOFError:
                raise Exception("Error in load experiment: Pickle was not able to retrieve the file.")
