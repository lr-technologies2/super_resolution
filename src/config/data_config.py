"""
 Data related (paths, preprocessing...) parameters
 Author: Adrien Dorise (adorise@lrtechnologies.fr) - LR Technologies
 Created: September 2023
 Last updated: Adrien Dorise - September 2023
"""

from sklearn.preprocessing import MinMaxScaler

main_path = r"data/debug_256/"

train_path = main_path + r"/debug_train/"
val_path = main_path + r"/debug_valid/"
test_path = main_path + r"debug_test/"
visu_path = main_path + r"debug_visu/"
save_path = r"models/tmp/dummy_experiment/"

#val_path = test_path = visu_path = train_path


input_shape = (256,256)
seq_length = 0
crop = None
scaler = None
nb_workers = 0

