"""
 Parameters for neural network applications
 Author: Adrien Dorise (adorise@lrtechnologies.fr) - LR Technologies
 Created: June 2023
 Last updated: Adrien Dorise - September 2023
"""

import torch

import src.architectures.CNN as CNN
import src.architectures.AE as AE
import src.architectures.VAE as VAE


import torch.nn as nn

class CustomLoss(nn.Module):
    def __init__(self):
        super(CustomLoss, self).__init__()

    def stft_snr(self, est, clean, eps=1e-8):
        s1 = est.reshape(est.size(0), -1)
        s2 = clean.reshape(clean.size(0), -1)
        s1_s2_norm = torch.sum(s1 * s2, -1, keepdim=True)
        s2_s2_norm = torch.sum(s2 * s2, -1, keepdim=True)
        s_target = s1_s2_norm / (s2_s2_norm + eps) * s2
        e_nosie = s1 - s_target
        target_norm = torch.sum(s_target * s_target, -1, keepdim=True)
        noise_norm = torch.sum(e_nosie * e_nosie, -1, keepdim=True)
        snr = 10 * torch.log10(target_norm / (noise_norm + eps) + eps)

        return (-(torch.mean(snr)) + 100) / 100

    def forward(self, input, target, eps=1e-8):
        loss = torch.mean((input - target)**2)
        snr = self.stft_snr(input, target, eps)
        
        return loss + snr / 10
    



input_size = 1024*1024*3
output_size = 1024*1024*3
NN_model = VAE.Variational_Auto_Encoders(input_size,output_size)


batch_size = 2
num_epoch = 100
lr = 1e-3
wd = 1e-3
optimizer = torch.optim.AdamW
crit = VAE.VAE_loss()

