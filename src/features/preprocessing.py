"""
 Preprocessing toolbox for the super resolution toolbox
 Author: Adrien Dorise (adorise@lrtechnologies.fr) - LR Technologies
 Created: September 2023
 Last updated: Adrien Dorise - September 2023
"""

import os

def loader(low_res_folder_path,
           high_res_folder_path, 
           batch_size=16, 
           no_batch=False, 
           shuffle=True, 
           scaler=None, 
           num_workers=1):
    """Create Pytorch DataLoader object from array dataset
    
    Args:
        low_res_folder_path  (string): Path to the folder containing low resolution pictures (ex: dataset/data1)
        high_res_folder_path (string): Path to the folder containing high resolution pictures (ex: dataset/data1)
        batch_size (int, optional): batch_size used for DataLoader. Defaults to 16.
        no_batch (bool, optional): False if user wants trainset/testset divided in mini batches (mostly used to train neural networks). True if user want a unqiue batch for all set (mostly used for machine learning). Default is False
        shuffle (bool, optional): True to shuffle the dataset. Defaults to True
        scaler (sklearn.preprocessing): Type of scaler used for the data (ex: MinMaxScaler()). Put None if you do not want to scale the data. If the scaler is not fitted yet, it will be on the data given with folder_path. Default to None.
        num_workers (int, optional): How many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. Default to 0
    Returns:
        dataset: Pytorch DataLoader object
        scaler (sklearn.preprocessing object): Scaler that was used to fit the features. Use it to fit other dataset instances
    """

    low_res = os.listdir(low_res_folder_path)


    videos = [folder_path+'/'+vid for vid in videos if vid.endswith(".avi")]
    features, targets = multi_file_set(videos)
    
    features = select_points(features, coords=coords, version=tracker_version)
    print(f"Feature selection: shape={features.shape} - Targets shape={targets.shape}")
    if(scaler is not None):
        try:
            features = scaler.transform(features)
        except NotFittedError as e:
            features = scaler.fit_transform(features)
        

    features = torch.tensor(features, dtype=torch.float32)
    targets = torch.tensor(targets, dtype=torch.float32)
    
    if(shuffle and not temporal):
        features, targets = utils.shuffle(features, targets)
    
    if(no_batch):
        batch_size = np.shape(features)[0]
    
    if(temporal):
        dataset = Sequential_Dataset(features,targets,sequence_length) 
    else:
        dataset = Dataset(features,targets)
   
    loader = DataLoader(dataset, batch_size=batch_size, num_workers=num_workers)
    return loader,scaler