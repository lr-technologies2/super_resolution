'''
image_processing.py integrates the methods used to create a data base.
Author: Adrien Dorise - Edouard Villain({adorise, evillain, jcohen}@lrtechnologies.fr) - LR Technologies
Created: September 2023
Last updated: Adrien Dorise - September 2023

The super_resolution applicaiton aim to create higher resolution images from low resolution pictures. 
Therefore, these two type of picture are to be integrated in the data base.
This script aim to create low_resolution images from high resolution ones in order to get the two image types.
'''

import numpy as np
from imgaug import augmenters as iaa
import cv2
import os


def decrease_resolution(high_resolution_img_path, new_resolution = [128,128], save_path = "output/low_res.png"):
    """decrease_resolution take a high resolution image and transform it to a specified resolution.
    Warning: If the new resolution is not dividing the original size, the closest integer is selected

    Args:
        high_resolution_img_path (string): Path to the high image resolution to transform
        new_resolution (list (sizeX, sizeY)): New image resolution. Defaults to [128,128].
    Return:
        Return the low resolution image
    """
    
    high_res_img = cv2.imread(high_resolution_img_path)
    original_size = high_res_img.shape[0:2]
    scaleX = original_size[0] / new_resolution[0]
    scaleY = original_size[1] / new_resolution[1]
    low_res_img = high_res_img[::int(scaleX),::int(scaleY)]
    
    cv2.imwrite(save_path, low_res_img)    
    return low_res_img


def set_resolution(img_path, new_resolution = [1024,1024], save_path = "output/resized.png"):
    """set_resolution resize an image to the desired resolution.

    Args:
        img_path (string): Path to the high image resolution to transform
        new_resolution (list (sizeX, sizeY)): New image resolution. Defaults to [1024,1024].
    Return:0.
        Return the transformed image
    """
    
    img = cv2.imread(img_path)
    img  = cv2.resize(img, dsize=(new_resolution[0], new_resolution[1]), interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(save_path,img)
    return img
    
    
def create_low_res_dataset(high_res_dataset_path, save_path, high_resolution = [1024,1024], low_res = [128,128]):
    """Create a low resolution dataset from a high resolution dataset

    Args:
        high_res_dataset_path (string): Folder path to the high resolution dataset (ex: "my/amazing/dataset/")
        save_path (string): Folder path to the new low resolution dataset (ex: "my/blurry/dataset/")
        high_resolution (list (2), optional): Desired input size. Defaults to [1024,1024].
        low_res (list (2), optional): Desired low resolution. Defaults to [128,128].
    """
    tmp_path = "output/tmp.png"
    for img_name in os.listdir(high_res_dataset_path):
        set_resolution(high_res_dataset_path + img_name, high_resolution, tmp_path)
        decrease_resolution(tmp_path,low_res,tmp_path)
        set_resolution(tmp_path, high_resolution, save_path + img_name)
        
        
    
    
if __name__ == "__main__":
    """
    img_path = "data/sharp_face/1 (1).jpeg"
    cv2.imwrite("output/high_res.jpg", cv2.imread(img_path))
    set_resolution(img_path)    
    decrease_resolution("output/resized.png")
    set_resolution("output/low_res.png", save_path = "output/resized2.jpg")
    """

    img_path = "data/debug_sharp_256_to_512/debug_visu/" #Sharp images folder
    
    high_res = 1024 #Base resolution for high res images
    low_res = 1024 #Base resolution for low res images
    default_high_res = 512 #Target resolution in the dataset
    default_low_res = 256 #Feature resolution in the dataset
    
    idx = 0
    img = [img_path + "targets/" + path for path in os.listdir(img_path+"targets/")]
    for i in img:
        target_path = i
        feature_path = img_path+"features/"+os.listdir(img_path+"targets/")[idx]
        
        set_resolution(target_path,new_resolution=[high_res,high_res],save_path=target_path)
        decrease_resolution(target_path,[low_res,low_res],save_path=feature_path)
        set_resolution(target_path,new_resolution=[default_high_res,default_high_res],save_path=target_path)
        set_resolution(feature_path,new_resolution=[default_low_res,default_low_res],save_path=feature_path)
        idx+=1
    #create_low_res_dataset("data/sharp_face/", "data/blurry_face/")