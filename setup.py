from setuptools import setup, find_packages

setup(name='super_resolution',
      version='0.4.0',
      description='Super resolution for face and eyes of a user using a webcam',
      author='LR Technologies - Boris Lenseigne, Julia Cohen, Adrien Dorise, Edouard Villain',
      author_email='{blenseigne, jcohen, adorise, evillain}@lrtechnologies.fr',
      url='https://gitlab.com/lr-technologies2/super_resolution',
     )