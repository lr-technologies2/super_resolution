'''
This is is LR Technologies Super Resolution application.
Last Update by Adrien Dorise - August 2023
'''



if __name__ == "__main__":
	from src.config.ML_config import *
	from src.config.NN_config import *
	from src.config.data_config import *
	from src.experiment import *

 
	experiment = Experiment(NN_model,
			train_path,
			val_path,
			test_path,
			visu_path,
			input_shape=input_shape,
			crop = crop,
			sequence_length=seq_length,
			num_epoch=num_epoch,
			batch_size=batch_size,
			learning_rate=lr,
			weight_decay=wd,
			optimizer=optimizer,
			criterion=crit,
			scaler=scaler,
			nb_workers=nb_workers)

	experiment.model.printArchitecture((1,3,input_shape[0],input_shape[1]))
	experiment.fit()
	experiment = Experiment.load("models/tmp/experiment")
	experiment.predict()
	experiment.visualise()

	